# Reconocimiento de caras
Código tomado de https://github.com/LuisAlejandroSalcedo/Proyecto-R-Face. Se agregaron fotos de entrenamiento (./train/)
Recomendación: instalar anaconda.

Entrenamiento:
python train.py

Reconocimiento, ejemplo:
python reconocer.py --image test/barack.jpg

Si da errores (relacionados a cv2, diciendo que face no es un atributo), instalar opencv-contrib:
pip3 install opencv-contrib-python